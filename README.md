<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Description
Template backend rest api menggunakan laravel

## License
- gratis untuk komersil
- siapapun bisa menggunakan source code ini 😁

## Author
radityo.hanif follow me in 
- instagram 👉 https://instagram.com/radityo.hanif
- gitlab    👉 https://gitlab.com/radityo.hanif

## Feature
### API
- Sistem autentikasi menggunakan json web token (JWT)
- API File Management
- API Login
- API Register
- API Forgot Password
- Email Service

### Database
- user_login
- file

## Installation
- setup file .env kamu
```bash
    # instalasi modul dan library yang dibutuhkan
    composer install
```
```bash
    # instalasi dan migrasi database
    php artisan migrate
```
```bash
    # menjalankan aplikasi pada port 8002
    php artisan serve --port 8002
```