<?php
namespace App;

class DBConstant
{
  const forbiddenQuery = [
    "drop",
    "delete",
    "alter",
    "truncate"
  ];

  const forbiddenVariableName = [
    "query",
    "variables",
  ];

  const defaultDriver = [
    'mysql' => [
      'name' => 'MySQL',
      'default_port' => 3306,
      'path_image' => '/public/driver/mysql.png',
    ],
    'pgsql' => [
      'name' => 'PostgreSQL',
      'default_port' => 5432,
      'path_image' => '/public/driver/pgsql.png',
    ],
    'google-big-query' => [
      'name' => 'Google Big Query',
      'default_port' => 443,
      'path_image' => '/public/driver/google-big-query.png',
    ]
  ];
}