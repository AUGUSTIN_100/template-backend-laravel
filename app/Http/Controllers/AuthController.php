<?php

namespace App\Http\Controllers;

use App\Util;
use App\Mail\ResetPasswordMail;
use App\Models\ActivationKey;
use App\Models\UserLogin;

use Carbon\Carbon;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function actionLogin(Request $request)
    {
        // validasi parameter
        $params = $request->all();
        $requiredParams = [
            "email",
            "password",
        ];
        $isNotValid = Util::validationRequiredParams($requiredParams, $params);
        if ($isNotValid) {
            return $isNotValid;
        }

        $defaultFailResult = Util::defaultResult("login gagal, silahkan cek kembali email dan password yang digunakan", 401);

        // cek apakah data user ada berdasarkan email
        $email = $params["email"];
        $model = UserLogin::where("login_email", $email)->first();
        if (!$model) {
            return $defaultFailResult;
        }

        // cek password
        $isPassword = Hash::check(
            $params["password"],
            $model->login_password
        );
        if (!$isPassword) {
            return $defaultFailResult;
        }

        // cek user expired
        $userExpired = Util::getArrOrObject($model, "login_effective_end_date");
        $currentDate = date('Y-m-d');
        if ($userExpired != null) {
            if ($userExpired <= $currentDate) {
                return Util::defaultResult("User sudah expired", 401);
            }
        }

        // cek apakah user aktif
        if ($model->login_status == 0) {
            $email = $model->login_email;
            return Util::defaultResult("Akun belum aktif silahkan cek email $email untuk melakukan aktivasi akun", 401);
        }

        // login berhasil
        // generate jwt
        $key = env("JWT_SECRET");  // baca variabel .env
        $payload = [
            "login_name" => $model->login_name,
            "login_email" => $model->login_email,
            "login_id" => $model->id,
            "login_role" => $model->login_type,
            "session_id" => Str::random(25)
        ];

        $jwt = JWT::encode($payload, $key, 'HS256');
        unset($payload["session_id"]);
        return response([
            "message" => "login berhasil",
            "jwt" => $jwt,
            "payload" => $payload,
            "status_code" => 200
        ]);
    }

    public function actionRegister(Request $request)
    {
        // validasi parameter
        $params = $request->all();
        $requiredParams = [
            "login_name",
            "login_email",
            "login_password",
        ];
        $paramAliases =  UserLogin::aliases;
        $isNotValid = Util::validationRequiredParams($requiredParams, $params, $paramAliases);
        if ($isNotValid) {
            return $isNotValid;
        }

        // validasi model
        $isNotValid = UserLogin::defaultValidation($params);
        if ($isNotValid) {
            return $isNotValid;
        }

        // proses simpan data model
        $model = new UserLogin();
        $model->login_name = Util::getArrOrObject($params, "login_name");;
        $model->login_email = Util::getArrOrObject($params, "login_email");;
        $model->login_password = Hash::make(Util::getArrOrObject($params, "login_password"));
        $model->login_type = 'user';
        $model->login_status = 1;
        $model->save();

        return Util::defaultResult("berhasil melakukan registrasi user baru", 200, $params);
    }

    public function actionRequestResetPassword(Request $request)
    {
        // validasi parameter
        $params = $request->all();
        $requiredParams = [
            "email",
        ];
        $isNotValid = Util::validationRequiredParams($requiredParams, $params);
        if ($isNotValid) {
            return $isNotValid;
        }

        // cek apakah data valid
        $email = Util::getArrOrObject($params, 'email');
        $user = UserLogin::where('login_email', $email)->whereNull('deleted_at')->first();
        $name = Util::getArrOrObject($user, 'login_name', 'Pengguna EMIS');

        if (!$user) {
            return Util::defaultResult(__FUNCTION__, 200);
        }

        // generate link aktivasi
        $requestLink = ActivationKey::create($user->id, activity: "reset-password");

        // kirim link aktivasi ke email yang bersangkutan
        $dataToMail = [
            'name' => $name,
            'link' => $requestLink,
        ];
        Mail::to($email)->send(new ResetPasswordMail($dataToMail));
        return Util::defaultResult(__FUNCTION__, 200);
    }

    public function actionResetPassword(Request $request)
    {
        // validasi parameter
        $params = $request->all();
        $requiredParams = [
            "activation_key",
            "new_password",
        ];
        $isNotValid = Util::validationRequiredParams($requiredParams, $params);
        if ($isNotValid) {
            return $isNotValid;
        }

        // cek apakah data valid
        $activationKey = ActivationKey::where('actk_key', Util::getArrOrObject($params, 'activation_key'))->whereNull('deleted_at')->first();
        if (!$activationKey) {
            return Util::unauthorizedResult();
        }

        $user = UserLogin::where('id', Util::getArrOrObject($activationKey, 'actk_login_fk'))->first();
        if (!$user) {
            return Util::unauthorizedResult();
        }

        $newPassword = Util::getArrOrObject($params, 'new_password');
        $newPassword = htmlspecialchars($newPassword, ENT_QUOTES, 'UTF-8');
        if (!Util::isStrongPassword($newPassword)) {
            return Util::defaultResult(
                message: "Password lemah. Pastikan memenuhi kriteria panjang, huruf besar, huruf kecil, angka, dan karakter khusus.",
                statusCode: 400
            );
        }

        if (!$newPassword) {
            return Util::unauthorizedResult();
        }

        DB::beginTransaction();
        try {
            // update data dengan password baru
            $user->login_password = Hash::make($newPassword);
            $user->save();

            // remove link aktivasi yang sudah digunakan
            $activationKey->deleted_at = Carbon::now();
            $activationKey->save();

            DB::commit();
            return Util::defaultResult(__FUNCTION__, 200);
        } catch (\Exception $e) {
            DB::rollback();
            $apiResponse = [
                "message" => "terjadi kesalahan pada aplikasi",
            ];
            if (!config('constant.IS_PROD')) {
                $apiResponse["error"] = $e->getMessage();
            }
            return Util::defaultResult($apiResponse, 500);
        }
    }
}
