<?php

namespace App\Http\Controllers;

use App\Util;
use Carbon\Carbon;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function create(Request $request)
    {
        // validasi parameter
        $params = $request->all();
        $requiredParams = [
            "course_name",
            "course_teacher",
            "course_desc",
        ];
        $paramAliases =  Course::aliases;
        $isNotValid = Util::validationRequiredParams($requiredParams, $params, $paramAliases);
        if ($isNotValid) {
            return $isNotValid;
        }

        // validasi model
        $isNotValid = Course::defaultValidation($params);
        if ($isNotValid) {
            return $isNotValid;
        }

        // proses simpan data model
        $model = new Course();
        $model->course_name = Util::getArrOrObject($params, "course_name");
        $model->course_teacher = Util::getArrOrObject($params, "course_teacher");
        $model->course_is_active = Util::getArrOrObject($params, "is_active", 1);
        $model->course_desc = Util::getArrOrObject($params, "course_desc");
        $model->course_label = Util::getArrOrObject($params, "course_label");
        $model->save();

        return Util::defaultResult("berhasil menambahkan data", 200, $params);
    }

    public function read(Request $request)
    {
        $enablePagination = false;

        $model = new Course();
        $params = $request->all();

        if (isset($params["pagination"])) {
            $enablePagination = true;
        }

        if ($enablePagination) {
            return Util::paginationBuilder($model, $params, ["name"]);
        }

        $data = $model->all();
        return Util::defaultResult("berhasil mengambil data", 200, $data, [
            "recordsTotal" => count($data)
        ]);
    }

    public function readOne($id)
    {
        $model = Course::where("id", $id)->first();
        if (!$model) {
            return Util::defaultResult("data untuk id $id tidak ditemukan", 404);
        }
        return Util::defaultResult("data untuk id $id berhasil ditemukan", 200, $model);
    }

    public function update(Request $request)
    {
        // validasi parameter
        $params = $request->all();
        $requiredParams = [
            "id",
        ];

        $isNotValid = Util::validationRequiredParams($requiredParams, $params);
        if ($isNotValid) {
            return $isNotValid;
        }

        // cek apakah data dengan id tersebut ada
        $id = $params["id"];
        $model = Course::find($id);
        if (!$model) {
            return Util::defaultResult("data dengan id $id tidak ditemukan", 404);
        }

        // validasi parameter ke model
        $isNotValid = Course::defaultValidation($params, $id);
        if ($isNotValid) {
            return $isNotValid;
        }

        // proses update data
        $updateStatus = false;
        $updateableFields = Course::updateableFields;

        foreach ($updateableFields as $field) {
            if (Util::isset($params, $field)) {
                $model = Course::where("id", $id)->update([
                    "$field" => Util::getArrOrObject($params, $field),
                    "updated_at" => Carbon::now(),
                ]);
                $updateStatus = true;
            }
        }

        if ($updateStatus) {
            return Util::defaultResult("berhasil mengupdate data dengan id $id", 200);
        }
        return Util::defaultResult("tidak ada data yang diupdate", 200);
    }

    public function delete($id)
    {
        $model = Course::find($id);
        if (!$model) {
            return Util::defaultResult("data dengan id $id tidak ditemukan", 404);
        }
        if (Util::getArrOrObject($model, "deleted_at") != null) {
            return Util::defaultResult("data dengan id $id sudah dihapus", 404);
        }
        $model = Course::where("id", $id)->update([
            "deleted_at" => Carbon::now()
        ]);
        return Util::defaultResult("berhasil menghapus data dengan id $id", 200);
    }
}
