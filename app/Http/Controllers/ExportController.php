<?php

namespace App\Http\Controllers;

use App\Models\Export;

class ExportController extends Controller
{
    public function excelDataUser()
    {
        return Export::sampleExportToExcel();
    }
}
