<?php

namespace App\Http\Controllers;

use App\Util;
use App\Models\SetupData;
use App\Models\UserLogin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class SetupDataController extends Controller
{
    public function actionInit()
    {
        if (SetupData::all()->count()) {
            return Util::defaultResult("setup data sudah dilakukan", 400);
        }

        DB::beginTransaction();
        try {
            // berikan tanda bahwa setup data sudah dilakukan
            $setupData = new SetupData();
            $setupData->save();

            // setup Akun Admin
            $adminEmail = env('ADMIN_DEFAULT_EMAIL', 'admin_default@yopmail.com');
            $adminPassword = env('ADMIN_DEFAULT_PASSWORD', 'password');
            $adminUser = new UserLogin();
            $adminUser->login_name = "Admin Default";
            $adminUser->login_email = $adminEmail;
            $adminUser->login_password = Hash::make($adminPassword);
            $adminUser->login_type = 'admin';
            $adminUser->login_status = 1;
            $adminUser->save();
            DB::commit();
            return Util::defaultResult("Berhasil melakukan setup data", 200);
        } catch (\Exception $e) {
            DB::rollback();
            $apiResponse = [
                "message" => "terjadi kesalahan pada aplikasi",
            ];
            if (!config('constant.IS_PROD')) {
                $apiResponse["error"] = $e->getMessage();
            }
            return Util::defaultResult($apiResponse, 500);
        }
    }

    public function actionCheckApplicationEnvironment()
    {
        return Util::defaultResult(
            message: __FUNCTION__,
            statusCode: 200,
            data: [
                'apps_env' => env('APP_ENV')
            ]
        );
    }
}
