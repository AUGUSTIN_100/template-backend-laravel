<?php

namespace App\Http\Controllers;

use App\Util;
use Carbon\Carbon;
use App\Models\UserLogin;

class UserLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function actionProfile()
    {
        $payload = Util::getJWTPayload();
        $id = $payload->login_id;
        $user = UserLogin::find($id);
        $result = [
            "login_name" => $user->login_name,
            "login_email" => $user->login_email,
            "login_created_at" => Carbon::parse($user->created_at)->format('d/M/Y'),
        ];
        return Util::defaultResult("berhasil mendapatkan data profil", 200, $result);
    }
}
