<?php

namespace App\Http\Middleware;

use App\Util;
use Closure;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Middleware untuk cek apakah role login saat ini adalah "Admin"
     */
    public function handle(Request $request, Closure $next)
    {
        $validRole = "admin";
        $jwtPayload = Util::getJWTPayload();
        $currentRole = Util::getArrOrObject($jwtPayload, "login_role");
        if (!$currentRole) {
            return Util::unauthorizedResult();
        }
        if ($validRole != $currentRole) {
            return Util::unauthorizedResult();
        }
        return $next($request);
    }
}
