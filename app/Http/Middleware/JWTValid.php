<?php

namespace App\Http\Middleware;

use App\Util;
use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;

class JWTValid
{
    /**
     * Middleware untuk cek apakah token JWT milik client valid ?
     */
    public function handle(Request $request, Closure $next)
    {
        $headers = $request->header();

        // Cek apakah client memiliki token
        if (!isset($headers["authorization"])) {
            return Util::unauthorizedResult();
        }

        // Cek apakah token milik client valid
        $clientToken = $headers["authorization"][0];
        $clientToken = explode(" ", $clientToken)[1];
        try {
            JWT::decode($clientToken, new Key(env("JWT_SECRET"), 'HS256'));
        } catch (\Exception $e) {
            return Util::unauthorizedResult();
        }

        // TODO: tambahkan expired session pada payload JWT
        // code here..

        return $next($request);
    }
}
