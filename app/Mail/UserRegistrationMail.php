<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistrationMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(env("APP_NAME") . " Detail Akun Baru Anda Harap Dikonfirmasi.")
            ->markdown('mail.user-registration', [
                "name" => $this->data["name"],
                "email" => $this->data["email"],
                "password" => $this->data["password"],
            ]);
    }
}
