<?php

namespace App\Models;

use Illuminate\Support\Str;
use Carbon\Carbon;

class ActivationKey extends AAAModel
{
    public static function create($login_fk, $activity = "activation")
    {
        // cek apakah user terkait sudah memiliki link aktivasi
        $key = self::generateKey();

        // update data link aktivasi lama
        if (self::hasActivationKey($login_fk)) {
            ActivationKey::where("actk_login_fk", $login_fk)->update([
                "actk_key" => $key,
                "updated_at" => Carbon::now(),
                "deleted_at" => null
            ]);
        } else {
            // buat data baru link aktivasi
            $model = new ActivationKey();
            $model->actk_key = $key;
            $model->actk_login_fk = $login_fk;
            $model->save();
        }

        return env("FRONTEND_URL", "http://localhost:5173") . "/$activity?key=" . $key;
    }

    public static function hasActivationKey($login_fk)
    {
        $model = self::where(["actk_login_fk" => $login_fk])->first();
        if ($model) {
            return true;
        }
        return false;
    }

    public static function generateKey()
    {
        $key = Str::random(25);
        $model = self::where(["actk_key" => $key])->first();
        if (!$model) {
            // jika key belum terdaftar, gunakan key
            return $key;
        }
        // jika key sudah terdaftar, generate key lainnya
        return self::generateKey();
    }
}
