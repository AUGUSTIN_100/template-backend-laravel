<?php

namespace App\Models;

use App\Util;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Course extends AAAModel
{
    use HasFactory;

    protected $casts = [
        "deleted_at" => 'datetime:Y-m-d H:m:s',
        "updated_at" => 'datetime:Y-m-d H:m:s',
        "created_at" => 'datetime:Y-m-d H:m:s',
    ];

    const updateableFields = [
        "course_name",
        "course_teacher",
        "course_is_active",
        "course_desc",
        "course_label",
    ];

    const aliases = [
        "course_name" => "Nama Course",
        "course_teacher" => "Nama Guru",
        "course_is_active" => "Status Aktif Course",
        "course_desc" => "Deskripsi Course",
        "course_label" => "Label Course",
    ];

    public static function isAvailable($field, $param, $id = false)
    {
        $model = self::where($field, $param);
        if ($id) {
            $model = $model->where("id", "!=", $id);
        }
        $model = $model->first();
        if (!$model) {
            return true;
        }
        return false;
    }

    public static function defaultValidation($params, $id = false)
    {
        $param = "course_name";
        if (Util::isset($params, $param)) {
            if (!self::isNameAvailable($params[$param], $id)) {
                return Util::defaultResult("nama course sudah terdaftar silahkan gunakan nama lainnya", 400);
            }
        }
        return false;
    }

    public static function isNameAvailable($param, $id = false)
    {
        return self::isAvailable("course_name", $param, $id);
    }
}
