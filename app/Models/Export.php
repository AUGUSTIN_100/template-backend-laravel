<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Util;

class Export extends Model
{
    public static function sampleExportToExcel()
    {
        // Sheet 1 : Detail Data
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle("Detail Data");
        $worksheet->setCellValue("A1", "Nama user");
        $worksheet->setCellValue("B1", "Email");
        $worksheet->setCellValue("C1", "Terakhir diubah");

        $model = new UserLogin();
        $model = $model->all();
        $sheetMap = [
            [
                "cell" => "A",
                "value" => "login_name"
            ],
            [
                "cell" => "B",
                "value" => "login_email"
            ],
            [
                "cell" => "C",
                "value" => "updated_at"
            ],
        ];

        // Fill worksheet with data
        foreach ($sheetMap as $map) {
            $cell = $map["cell"];
            $value = $map["value"];
            for ($i = 0; $i < count($model); $i++) {
                $indexSheet = $i + 2;
                $indexSheet = $cell . $indexSheet;
                $worksheet->setCellValue($indexSheet, $model[$i][$value]);
            }
        }

        // Sheet 2 : Summary
        $worksheet2 = $spreadsheet->createSheet();
        $worksheet2->setTitle("Summary");
        $dataSummary = [
            "Jumlah User" => count($model),
            "Deskripsi" => "diketahui saat ini terdapat " . count($model) . " jumlah user pada aplikasi"
        ];
        $worksheet2->setCellValue("A1", "Jumlah User");
        $worksheet2->setCellValue("A2", "Deskripsi");

        $worksheet2->setCellValue("B1", $dataSummary["Jumlah User"]);
        $worksheet2->setCellValue("B2", $dataSummary["Deskripsi"]);

        $spreadsheet = Util::formatExcel($spreadsheet);
        $writer = new Xlsx($spreadsheet);
        $path = 'excel/data_user.xlsx';
        $writer->save($path);

        return response()->download($path, "data_user.xlsx");
    }
}
