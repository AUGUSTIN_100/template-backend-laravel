<?php

namespace App\Models;

use Illuminate\Support\Str;

class File extends AAAModel
{
    const IMAGES_FORMAT = ["jpg", "png", "jpeg"];

    public static function generateFileKeychar()
    {
        $key = Str::random(25);
        $model = self::where(["file_keychar" => $key])->first();
        if (!$model) {
            return $key;
        }
        return self::generateFileKeychar();
    }

    public static function getFileIdByKeychar($keychar)
    {
        $keychar = str_replace("\"", "", $keychar);
        $model = self::where(["file_keychar" => $keychar])->first();
        if (!$model) {
            return null;
        }
        return $model->id;
    }

    public static function getFileUrlByKeychar($keychar)
    {
        $model = self::where("file_keychar", $keychar)->first();
        if (!$model) {
            return null;
        }
        return url($model->file_path);
    }

    public static function getFileUrlById($id)
    {
        $model = self::where("id", $id)->first();
        if (!$model) {
            return null;
        }
        return url($model->file_path);
    }

    public static function isImageFile($fileName)
    {
        $fileName = explode(".", $fileName);
        if (in_array($fileName[1], self::IMAGES_FORMAT)) {
            return true;
        }
        return false;
    }
}
