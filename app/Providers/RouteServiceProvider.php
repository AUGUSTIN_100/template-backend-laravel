<?php

namespace App\Providers;

use App\Util;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });
    }

    protected function logAccess($request, $userId = '', $sessionId = '')
    {
        Log::info(">>>>>>>> Start Log Access >>>>>>>>");
        Log::info("Request Routes: " . $request->url());
        Log::info("Request IP: " . $request->ip());
        Log::info("User ID: " . $userId);
        Log::info("Session ID: " . $sessionId);
        Log::info(">>>>>>>> End Log Access >>>>>>>>");
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('by_session_id', function (Request $request) {
            $sessionId = Util::getSessionId();
            $userId = Util::getUserId();
            $this->logAccess(
                request: $request,
                userId: $userId,
                sessionId: $sessionId
            );
            return Limit::perMinute(env('RATE_LIMIT_BY_SESSION_ID', 60))->by($sessionId)->response(function () {
                return Util::defaultResult('Anda telah mencapai batas akses Anda. Silakan coba setelah 1 menit.', 429);
            });
        });

        RateLimiter::for('public', function (Request $request) {
            $xsrfToken = $request->cookie('XSRF-TOKEN');
            $this->logAccess(
                request: $request
            );
            return Limit::perMinute(env('RATE_LIMIT_PUBLIC', 60))->by($xsrfToken)->response(function () {
                return Util::defaultResult('Anda telah mencapai batas akses Anda. Silakan coba setelah 1 menit.', 429);
            });
        });
    }
}
