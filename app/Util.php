<?php

namespace App;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Util
{
    public static function formatBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');
        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)] . 'B';
    }

    public static function getJWTPayload()
    {
        $headers = getallheaders();
        try {
            if (isset($headers["Authorization"])) {
                $clientToken = $headers["Authorization"];
            } else if (isset($headers["authorization"])) {
                $clientToken = $headers["authorization"];
            }
            $clientToken = explode(" ", $clientToken)[1];
            $payload = JWT::decode($clientToken, new Key(env("JWT_SECRET"), 'HS256'));
            return $payload;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getSessionId()
    {
        $jwtPayload = self::getJWTPayload();
        return self::getArrOrObject($jwtPayload, "session_id");
    }

    public static function getUserId()
    {
        $jwtPayload = self::getJWTPayload();
        return self::getArrOrObject($jwtPayload, "login_id");
    }

    public static function formatExcel($spreadsheet)
    {
        // Auto size columns for each worksheet
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            $spreadsheet->setActiveSheetIndex($spreadsheet->getIndex($worksheet));
            $sheet = $spreadsheet->getActiveSheet();
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            foreach ($cellIterator as $cell) {
                $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            }
        }
        return $spreadsheet;
    }

    public static function getNextSequence($tableName)
    {
        $id = DB::select("SHOW TABLE STATUS LIKE '$tableName'");
        $nextId = $id[0]->Auto_increment;
        return $nextId;
    }

    public static function goOutJson($data, $responseCode = 500)
    {
        // Fungsi untuk debugging, direkomendasikan menggunakan postman atau tools sejenisnya untuk mengunakan fungsi ini
        header('Content-Type: application/json; charset=utf-8');
        http_response_code($responseCode);
        $data["status_code"] = $responseCode;
        echo json_encode($data);
        exit();
    }

    public static function unauthorizedResult($message = false)
    {
        if (!$message) {
            $message = "kamu tidak memiliki akses";
        }
        return self::defaultResult($message, 401);
    }

    public static function debugResult($data, $simpleMode = false)
    {
        $statusCode = 400;
        $apiResponse = [
            "message" => "fitur ini sedang dalam proses perbaikan",
            "status_code" => $statusCode
        ];
        if ($simpleMode) {
            $apiResponse["debug"] = [
                "data" => $data,
                "data_type" => gettype($data)
            ];
        } else {
            $apiResponse = array_merge($apiResponse, $data);
        }
        return response($apiResponse, $statusCode);
    }

    public static function defaultResult(
        $message,
        $statusCode,
        $data = false,
        $additionalData = false,
        $rawResult = false,
    ) {
        $apiResponse = [
            "message" => $message,
            "status_code" => $statusCode,
            "status" => ($statusCode == 200) ? true : false,
        ];

        if ($data) {
            $apiResponse["data"] = $data;
        } else {
            if (is_array($data)) {
                $apiResponse["data"] = [];
            }
        }

        if (!config('constant.IS_PROD')) {
            $apiResponse["dev_mode"] = true;
        }

        if ($additionalData) {
            $apiResponse = array_merge($additionalData, $apiResponse);
        }

        if ($rawResult) {
            return $apiResponse;
        }

        return response(
            $apiResponse,
            $statusCode
        );
    }

    public static function isset($arrayOrObject, $key)
    {
        $param = self::getArrOrObject($arrayOrObject, $key);
        if ($param) {
            return isset($param) && ($param != '') && ($param != null) && ($param != "null");
        }
        return false;
    }

    public static function getArrOrObject($arrayOrObject, $key, $default = null)
    {
        $res = $default;
        if (is_array($arrayOrObject)) {
            $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
            if ($res == null) {
                $key = strtoupper($key);
                $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
                if ($res == null) {
                    $key = strtolower($key);
                    $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
                }
            }
        } else {
            if (is_object($arrayOrObject)) {
                if (property_exists($arrayOrObject, $key) || isset($arrayOrObject->$key)) {
                    $res = $arrayOrObject->$key;
                }
            }
        }
        if ($res == 'undefined') {
            return $default;
        }
        return $res;
    }

    public static function paginationBuilder($model, $params, $searchFields = [], $customCallback = false, $defaultLength = 10)
    {
        $dataPagination = [];
        $start = isset($params["start"]) ? (int) $params["start"] : 0;
        $length = isset($params["length"]) ? (int) $params["length"] : $defaultLength;
        $searchParam = isset($params["search"]["value"]) ? $params["search"]["value"] : false;
        $searchParam = isset($params["search"]) ? $params["search"] : false;

        // Jika sedang dalam mode pencarian
        if ($searchParam) {
            if (count($searchFields) != 0) {
                if (count($searchFields) == 1) {
                    $dataPagination = $model->where($searchFields[0], "LIKE", "%$searchParam%");
                } else {
                    $dataPagination = $model->where($searchFields[0], "LIKE", "%$searchParam%");
                    for ($i = 0; $i < count($searchFields); $i++) {
                        if ($i >= 1) {
                            $dataPagination->orWhere($searchFields[$i], "LIKE", "%$searchParam%");
                        }
                    }
                }
            }
            $dataPagination->limit($length)->offset($start);
            $recordsTotal = $dataPagination->count();
            $dataPagination = $dataPagination->get();
        } else {
            $dataPagination = $model->limit($length)->offset($start);
            $recordsTotal = $dataPagination->count();
            $dataPagination = $dataPagination->get();
        }

        if ($customCallback) {
            $dataPagination = call_user_func($model, $customCallback);
        }

        $apiResponse = [
            "data" => $dataPagination,
            "start" => $start,
            "length" => $length,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsTotal,
            "status_code" => 200
        ];

        return response($apiResponse, 200);
    }

    public static function validationRequiredParams($requiredParams, $params, $paramAliases = false)
    {
        foreach ($requiredParams as $param) {
            if (self::isset($params, $param)) {
                if ($params[$param] == null) {
                    if ($paramAliases) {
                        $paramName = $paramAliases[$param];
                    } else {
                        $paramName = str_replace("_", " ", $param);
                    }
                    return self::defaultResult("parameter $paramName wajib diisi", 400, false, false);
                }
            } else {
                $paramName = str_replace("_", " ", $param);
                return self::defaultResult("parameter $paramName wajib diisi", 400, false, false);
            }
        }
    }

    public static function isAvailable($model, $field, $param, $id = false, $isString = true)
    {
        if ($isString) {
            $param = strtolower($param);
            $field = DB::raw("LOWER($field)");
        }
        $data = $model::where($field, $param)->whereNull('deleted_at');

        if ($id) {
            $data = $data->where("id", "!=", $id);
        }
        $data = $data->first();
        if (!$data) {
            return true;
        }
        return false;
    }

    public static function generateUniqueID($model, $field, $length = 12)
    {
        $uid = Str::random($length);
        $model = $model::where([$field => $uid])->first();
        if (!$model) {
            return $uid;
        }
        return self::generateUniqueID($model, $field);
    }

    public static function formatRupiah($value)
    {
        $result = "Rp." . number_format($value, 0, ',', '.');
        return $result;
    }

    public static function formatAngka($value)
    {
        $result = number_format($value, 0, ',', '.');
        return $result;
    }

    public static function createSlug($string)
    {
        $string = strtolower($string);
        return str_replace(' ', '-', $string);
    }

    public static function in_2array($needle, $haystack, $full_search = false)
    {
        $ret = false;
        foreach ($needle as $search) {
            if (in_array($search, $haystack)) {
                $ret = true;
                if (!$full_search) {
                    break;
                }
            } else if ($full_search) {
                $ret = false;
                break;
            }
        }
        return $ret;
    }

    const months = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember",
    ];

    public static function getMonthName($index)
    {
        return self::getArrOrObject(self::months, $index - 1, "-");
    }

    public static function preprocessNullString($params)
    {
        foreach ($params as &$one) {
            if ($one == "null") {
                $one = null;
            }
        }
        return $params;
    }

    public static function renameParams($params, $aliases)
    {
        $aliasKeys = array_keys($aliases);
        foreach ($aliasKeys as $oldKey) {
            if (isset($params[$oldKey])) {
                $newKey = $aliases[$oldKey];
                $params[$newKey] = $params[$oldKey];
                unset($params[$oldKey]);
            }
        }
        return $params;
    }

    public static function getCsrfToken()
    {
        return self::getArrOrObject($_COOKIE, "XSRF-TOKEN");
    }

    public static function getClientIp()
    {
        $headers = getallheaders();
        return self::getArrOrObject($headers, "X-Real-IP", null);
    }

    public static function isStrongPassword($password)
    {
        // Panjang minimal 8 karakter
        if (strlen($password) < 8) {
            return false;
        }

        // Setidaknya satu huruf besar
        if (!preg_match('/[A-Z]/', $password)) {
            return false;
        }

        // Setidaknya satu huruf kecil
        if (!preg_match('/[a-z]/', $password)) {
            return false;
        }

        // Setidaknya satu angka
        if (!preg_match('/[0-9]/', $password)) {
            return false;
        }

        // Jika semua kriteria terpenuhi, password dianggap kuat
        return true;
    }

    public static function isContainString($haystack, $needle)
    {
        if (is_array($needle)) {
            foreach ($needle as $substring) {
                if (strpos($haystack, $substring) !== false) {
                    return true; // Jika salah satu substring ditemukan, kembalikan true
                }
            }
        } else {
            if (strpos($haystack, $needle) !== false) {
                return true;
            }
        }
        return false; // Jika tidak ada substring yang ditemukan, kembalikan false
    }
}
