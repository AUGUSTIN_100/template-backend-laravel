@component('mail::message')
# {{$name}}

Terima kasih telah mendaftar. Kami sangat senang Anda bergabung dengan kami. Untuk mengakses akun Anda, gunakan detail login yang Anda daftarkan saat registrasi.<br>

Email: {{$email}}<br>
Kata Sandi: {{$password}}<br>

@component('mail::button', ['url' => env('FRONTEND_URL', 'http://localhost:5173') . '/login'])
Login disini
@endcomponent

Jangan ragu untuk mengeksplorasi fitur-fitur kami dan jangan sungkan untuk menghubungi kami jika Anda memiliki pertanyaan atau masalah.<br>

Atas perhatiannya, Terima kasih<br>
{{ config('app.name') }}
@endcomponent
