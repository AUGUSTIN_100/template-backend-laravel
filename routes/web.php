<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\SetupDataController;
use App\Http\Controllers\UserLoginController;

Route::get('/', function () {
    return env('APP_NAME', '[Laravel Template Backend]');
});

/*
|--------------------------------------------------------------------------
| Util Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['throttle:public'])->group(function () {
    Route::get("/setup-data", [SetupDataController::class, "actionInit"]);
    Route::get("/check/env", [SetupDataController::class, "actionCheckApplicationEnvironment"]);
});


/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['throttle:public'])->group(function () {
    Route::post("/login", [AuthController::class, "actionLogin"]);
    Route::post("/register", [AuthController::class, "actionRegister"]);
    Route::post("/reset-password/request", [AuthController::class, "actionRequestResetPassword"]);
    Route::post("/reset-password", [AuthController::class, "actionResetPassword"]);
});

/*
|--------------------------------------------------------------------------
| User Login Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['throttle:public'])->group(function () {
    Route::get("/user/profile", [UserLoginController::class, "actionProfile"]);
});

/*
|--------------------------------------------------------------------------
| File Routes
|--------------------------------------------------------------------------
*/
Route::middleware(['throttle:public'])->group(function () {
    Route::post("/file/upload", [FileController::class, "create"]);
    Route::post("/file", [FileController::class, "read"]);
});

/*
|--------------------------------------------------------------------------
| Course Routes
| Sample CRUD (Create, Read, Update, Delete)
|--------------------------------------------------------------------------
*/
Route::middleware(['jwt', 'admin', 'throttle:by_session_id'])->group(function () {
    Route::post("/course/create", [CourseController::class, "create"]);
    Route::post("/course", [CourseController::class, "read"]);
    Route::get("/course/{id}", [CourseController::class, "readOne"]);
    Route::put("/course", [CourseController::class, "update"]);
    Route::delete("/course/{id}", [CourseController::class, "delete"]);
});
